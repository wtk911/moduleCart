package net.flowas.secure.rest;

import javax.servlet.ServletConfig;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

public class CXFNonSpringServletImpl extends CXFNonSpringServlet {

	private static final long serialVersionUID = 8262880864551976903L;

	@Override
	public void loadBus(ServletConfig servletConfig) {
		super.loadBus(servletConfig);
		SomeEndpoint helloworldImpl = new SomeEndpoint();
		JAXRSServerFactoryBean svrFactory = new JAXRSServerFactoryBean();
		svrFactory.setServiceClass(SomeEndpoint.class);
		JacksonJsonProvider json=new JacksonJsonProvider();
		svrFactory.setProvider(json);
		//svrFactory.setAddress("/helloService");
		svrFactory.setServiceBean(helloworldImpl);
	    svrFactory.create();	
	}
}