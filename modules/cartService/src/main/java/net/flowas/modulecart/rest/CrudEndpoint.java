package net.flowas.modulecart.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

public interface CrudEndpoint<T> {
	@POST
	@Consumes("application/json")
	T create(T entity);

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	void deleteById(@PathParam("id") Long id);
	
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces("application/json")
	T findById(@PathParam("id") Long id);

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes("application/json")
	void update(T entity);

}