package maintenance;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;

public class Utils {

	public static void writeToTable(IDatabaseConnection dbconn, InputStream in)
			throws SQLException, DatabaseUnitException {
		final FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
		builder.setColumnSensing(true);
		IDataSet dataSet = builder.build(in);
		//if (dataSet.iterator().getTable().getRowCount() > 0) {
			DatabaseOperation.CLEAN_INSERT.execute(dbconn, dataSet);
		//}
	}

	public static IDatabaseConnection getConnection() throws Exception {
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost/modulecart", "postgres", "flowas");
		IDatabaseConnection connection = new DatabaseConnection(conn);
		return connection;
	}
}
